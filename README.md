# Autossh

Forked from https://hub.docker.com/r/jnovack/autossh (https://github.com/jnovack/docker-autossh) and extended it with an arm32v7 distribution.


## Server

login to root@85.214.111.161
```
vi edge_key.pub 
echo 'no-agent-forwarding,no-user-rc,no-X11-forwarding,no-pty' $(cat edge_key.pub) | sudo su -s /bin/bash sshtunnel -c "tee >> ~/.ssh/authorized_keys"
```

Ports:

* Technologiefabrik Karlsruhe Meetingroom H120

WEB 85.214.111.161:10278
SSH 85.214.111.161:10279

* Shared Office Landau

WEB 85.214.111.161:10280
SSH 85.214.111.161:10281

* SWE Schorndorf

WEB 85.214.111.161:10283
SSH 85.214.111.161:10282

* SWE Austausch 05

WEB 85.214.111.161:10285
SSH 85.214.111.161:10284


* EDOMO Haus 1

WEB 85.214.111.161:10287
SSH 85.214.111.161:10286

* 2. Seebacher EDGE 1.1

WEB 85.214.111.161:10288
SSH 85.214.111.161:10289

* SWE Austausch 06

WEB 85.214.111.161:10290
SSH 85.214.111.161:10291



## Sample configuration to proxy the dashboard from the EDOMO_stack

configure remote port here: XXXXX 

docker-compose-autossh.yml

```yml
version: "3.7"
services:
  autossh-web-to-docker-dashboard:
    image: registry.gitlab.com/edomo/services/autossh-service/autossh-service-arm32v7:1.0.4
    environment:
      - SSH_HOSTUSER=sshtunnel
      - SSH_HOSTNAME=85.214.111.161
      - SSH_TUNNEL_REMOTE=85.214.111.161:XXXXX
      - SSH_TUNNEL_HOST=dashboard
      - SSH_TUNNEL_LOCAL=80
    volumes:
      - /mnt/data/autossh/id_rsa:/id_rsa
    dns:
      - 8.8.8.8
      - 4.2.2.4
    deploy:
      restart_policy: { condition: on-failure }
    networks:
      - frontend

  autossh-ssh-to-docker-host:
    image: registry.gitlab.com/edomo/services/autossh-service/autossh-service-arm32v7:1.0.4
    environment:
      - SSH_HOSTUSER=sshtunnel
      - SSH_HOSTNAME=85.214.111.161
      - SSH_TUNNEL_REMOTE=85.214.111.161:XXXXX
      - SSH_TUNNEL_HOST=localhost
      - SSH_TUNNEL_LOCAL=22
    volumes:
      - /mnt/data/autossh/id_rsa:/id_rsa
    dns:
      - 8.8.8.8
      - 4.2.2.4
    deploy:
      restart_policy: { condition: on-failure }
    networks:
      - hostnet
networks: 
  frontend:
    name: EDOMO_frontend
    external: true
  hostnet:
    name: host
    external: true

volumes:
  auto-ssh: null
```

### Deploy it

Provide the file `/mnt/data/autossh/id_rsa` with a valid id_rsa (created on your local system, without a passphrase!).
And add the respective ports and id_rsa.pub in our autossh tunnel server.


```ssh-keygen -t rsa -b 4096 -C "autossh" -f ./new_id_rsa```

```bash
Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in ./new_id_rsa.
Your public key has been saved in ./new_id_rsa.pub.
The key fingerprint is:
SHA256:shgLWyOjgxtJLWa/u2ihDQh2sg8o7OXBFbQx5xHPdP4 autossh
```

Deploy the stack

```bash
docker stack deploy -c docker-compose-autossh.yml  EDOMO_AUTOSSH
```


If needed - check the logs with: 
```
docker service logs -f EDOMO_AUTOSSH_autossh-web-to-docker-dashboard
docker service logs -f EDOMO_AUTOSSH_autossh-ssh-to-docker-host
``` 
