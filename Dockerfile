ARG BUILD_DATE
ARG BUILD_ARCH
FROM $BUILD_ARCH/alpine

ARG BUILD_DATE
ARG VCS_REF
ADD /entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

ENV \
  TERM=xterm \
  AUTOSSH_LOGFILE=/dev/stdout \
  AUTOSSH_GATETIME=30 \
  AUTOSSH_POLL=10 \
  AUTOSSH_FIRST_POLL=30 \
  AUTOSSH_LOGLEVEL=1

RUN apk update && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community/" >>/etc/apk/repositories && apk add --update autossh && apk add --update openssh-client && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/entrypoint.sh"]
